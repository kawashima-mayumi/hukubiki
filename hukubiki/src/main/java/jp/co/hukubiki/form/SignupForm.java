package jp.co.hukubiki.form;

import org.hibernate.validator.constraints.NotEmpty
;

import jp.co.hukubiki.form.validator.ConfirmPassword;
@ConfirmPassword(password = "password", confirmPassword = "confirmPassword")
public class SignupForm {
	@NotEmpty
	private String name;
	@NotEmpty
	private String login_id;
	@NotEmpty
	private String password;
	@NotEmpty
	private String confirm_password;
//確認用パスワードが機能していない！


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLogin_id() {
		return login_id;
	}
	public String getPassword() {
		return password;
	}
	public String getConfirm_password() {
		return confirm_password;
	}


}
