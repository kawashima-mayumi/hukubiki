<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>福引ちゃん</title>
</head>
<body>
	<div>
	<c:if test="${ empty loginUser }">
		<a href="login" class="link">ログイン</a>
        <a href="signup"  class="link">登録する</a>
	</c:if>
	</div>

	<div>
	<c:if test="${ empty loginUser }">
		<form:form modelAttribute="loginForm" class="form" action="${pageContext.request.contextPath}/login" method="post">
		<div>ログインID<form:input path="login_id" /></div>
        <div>パスワード<form:password path="password" /></div>
        <input type="submit" value="ログイン">
		</form:form>
	</c:if>
	</div>


	<h2>${message}</h2>



</body>
</html>