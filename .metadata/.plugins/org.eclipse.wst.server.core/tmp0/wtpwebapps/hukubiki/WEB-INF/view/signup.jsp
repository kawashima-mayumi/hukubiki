<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録</title>
</head>
<body>
<h1>ユーザー登録をして下さい</h1>
	<form:form modelAttribute="signupForm" class="form">
		<div >ユーザー名<form:input path="name" /></div>
		<div>ログインID<form:input path="login_id" /></div>
        <div>パスワード<form:password path="password" /></div>
        <div>確認用パスワード<form:password path="confirm_password" /></div>
        <input type="submit" value="登録">
	</form:form>
</body>
</html>